db.users.insertMany([
			{
				"firstName" : "Ruben",
				"lastName" : "Caballero",
				"email" : "rubben.caballero@mail.com",
				"password" : "rubencaballero123",
				"isAdmin" : false,
			},
			{
				"firstName" : "Jovan",
				"lastName" : "Mabini",
				"email" : "jovan.mabini@mail.com",
				"password" : "jovanmabini312",
				"isAdmin" : false,
			},
			{
				"firstName" : "Fritz",
				"lastName" : "Labrado",
				"email" : "fritz.labrado@mail.com",
				"password" : "fritzlabrado356",
				"isAdmin" : false,
			},
			{
				"firstName" : "Cesar",
				"lastName" : "delos Santos",
				"email" : "cesar.delossantos@mail.com",
				"password" : "cesardelosSantos12312",
				"isAdmin" : false,
			},
			{
				"firstName" : "Jhunjie",
				"lastName" : "Dosdos",
				"email" : "jhunjie.dosdos@mail.com",
				"password" : "jhunjiedosdos878",
				"isAdmin" : false,
			}

		])


db.courses.insertMany([
			{
				"name" : "Financial Management",
				"price" : "4500",
				"isActive" : false,
			},
			{
				"name" : "Marketing Management",
				"price" : "2500",
				"isActive" : false,
			},
			{
				"name" : "Fundamentals in Programming",
				"price" : "9000",
				"isActive" : false,
			},


		])

db.users.find({"isAdmin" : false})
db.users.updateOne({}, {$set: {"isAdmin" : true}})
db.courses.updateOne({"name" : "Fundamentals in Programming"}, {$set: {"isActive" : true}})
db.courses.deleteMany({"isActive" : false})




